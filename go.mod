module bitbucket.org/mavz/metadata

go 1.14

require (
	github.com/dgraph-io/badger v1.6.2
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.7.4
	github.com/google/uuid v1.3.0
	github.com/joho/godotenv v1.4.0
	github.com/lib/pq v1.10.4
	github.com/recoilme/pudge v1.0.3
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.9.0
)
