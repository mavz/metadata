package service

import (
	"bitbucket.org/mavz/metadata/pkg/model"
	"bitbucket.org/mavz/metadata/pkg/repository"
)

type Authorization interface {
	//CreateUser(user model.User) (int, error)
	GenerateToken(username, password string) (string, error)
	ParseToken(token string) (string, error)
}

type Metadata interface {
	Update(metadata *[]model.Metadata) ([]string, error)
	UpdateOne(metadata *model.Metadata) error
	GetById(id string) (*model.Metadata, error)
}

type Service struct {
	Authorization
	Metadata
}

func NewService(repos *repository.Repository) *Service {
	return &Service{
		Authorization: NewAuthService(),
		Metadata:      NewMetadataService(repos.Metadata),
	}
}
