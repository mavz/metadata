package service

import (
	"bitbucket.org/mavz/metadata/pkg/model"
	"bitbucket.org/mavz/metadata/pkg/repository"
)

type MetadataService struct {
	repo repository.Metadata
}

func NewMetadataService(repo repository.Metadata) *MetadataService {
	return &MetadataService{repo: repo}
}

func (s *MetadataService) Update(metadata *[]model.Metadata) ([]string, error) {
	result := []string{}
	for _, m := range *metadata {
		err := s.UpdateOne(&m)
		if err != nil {
			continue
		}
		result = append(result, m.Id)
	}
	return result, nil
}

func (s *MetadataService) UpdateOne(metadataInput *model.Metadata) error {

	//ищем в репозитории документ метаданных
	metadataStok, err := s.repo.SearchOne(metadataInput.Id)
	if err != nil {
		return err
	}
	//если документ ещё не существует, то сохранить его полностью
	if metadataStok == nil {
		err = s.repo.WriteMetadata(*metadataInput)
		if err != nil {
			return err
		}
		return nil
	}
	//logrus.Info("Нашёлся документ метадаты в базе", metadataStok)
	//документ нашелся, объединяем поля перед сохранением

	err = metadataStok.Сompare(metadataInput)
	if err != nil {
		return err
	}
	//logrus.Info("Готовый для сохранения документ метадаты в базе", metadataStok)
	err = s.repo.WriteMetadata(*metadataStok)
	if err != nil {
		return err
	}
	return nil
}

func (s *MetadataService) GetById(id string) (*model.Metadata, error) {
	return s.repo.SearchOne(id)
}
