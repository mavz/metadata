package repository

import (
	"github.com/dgraph-io/badger"
)

type Config struct {
	PathDir  string
	ValueDir string
}

func NewBadgerDB(cfg Config) (*badger.DB, error) {
	opts := badger.DefaultOptions(cfg.PathDir)
	opts.ValueDir = cfg.ValueDir
	opts.NumVersionsToKeep = 1
	opts.MaxTableSize = 100000000
	opts.ValueLogFileSize = 100000000
	db, err := badger.Open(opts)
	if err != nil {
		return db, err
	}

	return db, nil
}
