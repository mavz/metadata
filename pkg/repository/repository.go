package repository

import (
	"bitbucket.org/mavz/metadata/pkg/model"
	"github.com/dgraph-io/badger"
)

type Authorization interface {
	CreateUser(user model.User) (int, error)
	GetUser(username, password string) (model.User, error)
}

type Metadata interface {
	SearchOne(id string) (*model.Metadata, error)
	WriteMetadata(metadata model.Metadata) error
}
type Repository struct {
	Metadata
}

func NewRepository(db *badger.DB) *Repository {
	return &Repository{
		Metadata: NewMetadataBadger(db),
	}
}
