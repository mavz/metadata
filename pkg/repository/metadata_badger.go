package repository

import (
	"bytes"
	"compress/gzip"
	"encoding/json"
	"io"
	"log"

	"bitbucket.org/mavz/metadata/pkg/model"
	"github.com/dgraph-io/badger"
	"github.com/sirupsen/logrus"
)

type MetadataBadger struct {
	db *badger.DB
}

func NewMetadataBadger(db *badger.DB) *MetadataBadger {
	return &MetadataBadger{db: db}
}

func (r *MetadataBadger) SearchOne(id string) (*model.Metadata, error) {

	var metadataJson []byte

	err := r.db.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(id))
		if err != nil {
			return err
		}
		metadataJson, err = item.ValueCopy(nil)
		return err
	})
	if err != nil {
		//logrus.Info("ошибка после поиска err  Key not found", err)
		//return err
	}
	if metadataJson == nil {
		logrus.Info("ничего не нашли", id)
		return nil, nil
	}
	uncompressedData, uncompressedDataErr := gUnzipData(metadataJson)
	if uncompressedDataErr != nil {
		log.Fatal(uncompressedDataErr)
	}

	var metadata model.Metadata
	err = json.Unmarshal(uncompressedData, &metadata)
	if err != nil {
		return nil, err
	}

	return &metadata, nil
}

func (r *MetadataBadger) WriteMetadata(metadata model.Metadata) error {

	metadataString, err := json.Marshal(metadata)
	if err != nil {
		return err
	}
	compressedData, compressedDataErr := gZipData(metadataString)
	if compressedDataErr != nil {
		log.Fatal(compressedDataErr)
	}
	err = r.db.Update(func(txn *badger.Txn) error {

		return txn.Set([]byte(metadata.Id), compressedData)
	})
	if err != nil {
		return err
	}
	return nil
}

func gUnzipData(data []byte) (resData []byte, err error) {
	b := bytes.NewBuffer(data)

	var r io.Reader
	r, err = gzip.NewReader(b)
	if err != nil {
		return
	}

	var resB bytes.Buffer
	_, err = resB.ReadFrom(r)
	if err != nil {
		return
	}

	resData = resB.Bytes()

	return
}

func gZipData(data []byte) (compressedData []byte, err error) {
	var b bytes.Buffer
	gz := gzip.NewWriter(&b)

	_, err = gz.Write(data)
	if err != nil {
		return
	}

	if err = gz.Flush(); err != nil {
		return
	}

	if err = gz.Close(); err != nil {
		return
	}

	compressedData = b.Bytes()

	return
}
