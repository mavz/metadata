package handler

import (
	"net/http"

	"bitbucket.org/mavz/metadata/pkg/model"
	"github.com/gin-gonic/gin"
)

func (h *Handler) updateMetadata(c *gin.Context) {

	var input []model.Metadata
	if err := c.BindJSON(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}
	//logrus.Info("данные получены", input)
	//result := "111"
	/**/
	result, err := h.services.Metadata.Update(&input)
	if err != nil {
		newErrorResponse(c, http.StatusNotFound, err.Error())
		return
	}
	/**/
	c.JSON(http.StatusOK, result)
}
