package handler

import (
	"bitbucket.org/mavz/metadata/pkg/service"

	"github.com/gin-gonic/gin"
)

type Handler struct {
	services *service.Service
}

func NewHandler(services *service.Service) *Handler {
	return &Handler{services: services}
}

func (h *Handler) InitRoutes() *gin.Engine {
	router := gin.New()

	auth := router.Group("/auth")
	{
		//auth.POST("/sing-up", h.singUp)
		auth.POST("/sing-in", h.singIn)
	}
	/**/
	api := router.Group("/api", h.userIdentity)
	{
		metadata := api.Group("/metadata")
		{
			metadata.GET("/:id", h.getMetadataById)
			metadata.POST("/update", h.updateMetadata)
			metadata.POST("/updateone", h.updateOneMetadata)
			//id.POST("/", h.getId)
		}
	}
	/**/
	return router
}
