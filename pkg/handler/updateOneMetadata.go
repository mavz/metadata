package handler

import (
	"net/http"

	"bitbucket.org/mavz/metadata/pkg/model"
	"github.com/gin-gonic/gin"
)

func (h *Handler) updateOneMetadata(c *gin.Context) {

	var input model.Metadata
	if err := c.BindJSON(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}
	//logrus.Info("данные получены", input)

	err := h.services.Metadata.UpdateOne(&input)
	if err != nil {
		newErrorResponse(c, http.StatusNotFound, err.Error())
		return
	}
	c.JSON(http.StatusOK, input)
}
