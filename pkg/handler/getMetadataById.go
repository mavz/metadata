package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func (h *Handler) getMetadataById(c *gin.Context) {

	id := c.Param("id")
	metadata, err := h.services.Metadata.GetById(id)
	if err != nil {
		newErrorResponse(c, http.StatusNotFound, err.Error())
		return
	}
	c.JSON(http.StatusOK, metadata)
}
