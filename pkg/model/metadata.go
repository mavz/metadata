package model

type Metadata struct {
	Id         string      `json:"id"`
	Data       Data        `json:"data"`
	MetaFields []MetaField `json:"meta_fields"`
}

type MetaField struct {
	ID    string `json:"id"`
	Name  string `json:"name"`
	Alias string `json:"alias"`
}

type Data struct {
	Tags []TagMeta `json:"tags"`
}

type TagMeta struct {
	Id         string `json:"id"`
	Value      string `json:"value"`
	DateUptate string `json:"date_uptate"`
}

func (ms *Metadata) Сompare(metadataInput *Metadata) error {
	//перебираем значение полей нового документа
	for _, t := range metadataInput.Data.Tags {
		if !ms.SearchTag(&t) {
			ms.Data.Tags = append(ms.Data.Tags, t)
		}
	}
	//перебираем метаинформацию о тегах нового документа
	for _, mf := range metadataInput.MetaFields {
		if !ms.SearchMetaField(&mf) {
			ms.MetaFields = append(ms.MetaFields, mf)
		}
	}
	return nil
}

func (m *Metadata) SearchTag(tag *TagMeta) bool {
	result := false
	//перебираем значение полей нового документа
	for _, t := range m.Data.Tags {
		//совпадение по id и значению должны совпадать
		if t.Id == tag.Id && t.Value == tag.Value {
			result = true
			break
		}
	}
	return result
}

func (m *Metadata) SearchMetaField(mf *MetaField) bool {
	result := false
	for _, m := range m.MetaFields {
		if m.ID == mf.ID {
			result = true
			break
		}
	}
	return result
}
